package itis.quiz.spaceships;

import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager {

    public CommandCenter() {}

    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        Spaceship selectedShip = null;
        int maxFirePower = 0;
        for (Spaceship ship : ships) {
            int firePower = ship.getFirePower();
            if (firePower > maxFirePower) {
                selectedShip = ship;
                maxFirePower = firePower;
            }
        }
        return selectedShip;
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        for (Spaceship ship : ships) {
            if (ship.getName().equals(name)) {
                return ship;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ArrayList<Spaceship> filteredList = new ArrayList<>();
        for (Spaceship spaceship : ships) {
            if (spaceship.getCargoSpace() >= cargoSize) {
                filteredList.add(spaceship);
            }
        }
        return filteredList;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> filteredList = new ArrayList<>();
        for (Spaceship spaceship : ships) {
            if (spaceship.getFirePower() == 0) {
                filteredList.add(spaceship);
            }
        }
        return filteredList;
    }
}