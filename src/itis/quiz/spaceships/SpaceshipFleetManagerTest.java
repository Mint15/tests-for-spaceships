package itis.quiz.spaceships;

import java.util.ArrayList;

public class SpaceshipFleetManagerTest {

    CommandCenter commandCenter = new CommandCenter();
    static float mark = 0;

    public static void main(String[] args) {
        SpaceshipFleetManagerTest spaceshipFleetManagerTest = new SpaceshipFleetManagerTest();

        boolean test1 = spaceshipFleetManagerTest.getMostPowerfulShip_returnShipTest();
        boolean test2 = spaceshipFleetManagerTest.getMostPowerfulShip_returnFirstTest();
        boolean test3 = spaceshipFleetManagerTest.getMostPowerfulShip_NoArmedShipsTest();
        boolean test4 = spaceshipFleetManagerTest.getShipByName_shipFoundTest();
        boolean test5 = spaceshipFleetManagerTest.getShipByName_shipNotFoundTest();
        boolean test6 = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_returnListTest();
        boolean test7 = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_returnEmptyListTest();
        boolean test8 = spaceshipFleetManagerTest.getAllCivilianShips_returnListTest();
        boolean test9 = spaceshipFleetManagerTest.getAllCivilianShips_returnEmptyListTest();

        if (test1) {
            System.out.println("Тест 1 прошел проверку. Метод getMostPowerfulShip выдает " +
                    "корабль с наибольшей боевой мощью");
            mark += 0.3;
        }else {
            System.out.println("Ошибка на тесте 1");
        }

        if (test2) {
            System.out.println("Тест 2 прошел проверку. Метод getMostPowerfulShip выдает именно первый корабль");
            mark += 0.3;
        }else {
            System.out.println("Ошибка на тесте 2");
        }

        if (test3) {
            System.out.println("Тест 3 прошел проверку. Метод getMostPowerfulShip выдает null," +
                    " если нужных кораблей нет");
            mark += 0.4;
        }else {
            System.out.println("Ошибка на тесте 3");
        }

        if (test4) {
            System.out.println("Тест 4 прошел проверку. Метод getShipByName работает, если искомый корабль существует");
            mark += 0.5;
        }else {
            System.out.println("Ошибка на тесте 4");
        }

        if (test5) {
            System.out.println("Тест 5 прошел проверку. Метод getShipByName работает, если искомый" +
                    " корабль не существует");
            mark += 0.5;
        }else {
            System.out.println("Ошибка на тесте 5");
        }

        if (test6) {
            System.out.println("Тест 6 прошел проверку. Метод getAllShipsWithEnoughCargoSpace выдает" +
                    " правильный список");
            mark += 0.5;
        }else {
            System.out.println("Ошибка на тесте 6");
        }

        if (test7) {
            System.out.println("Тест 7 прошел проверку. Метод getAllShipsWithEnoughCargoSpace выдает пустой список");
            mark += 0.5;
        }else {
            System.out.println("Ошибка на тесте 7");
        }

        if (test8) {
            System.out.println("Тест 8 прошел проверку. Метод getAllCivilianShips выдает правильный список");
            mark += 0.5;
        }else {
            System.out.println("Ошибка на тесте 8");
        }

        if (test9) {
            System.out.println("Тест 9 прошел проверку. Метод getAllCivilianShips выдает пустой список");
            mark += 0.5;
        }else {
            System.out.println("Ошибка на тесте 9");
        }

        System.out.println("Всего баллов: " + mark);
    }

    private boolean getMostPowerfulShip_returnShipTest() {
        ArrayList<Spaceship> testingList = new ArrayList<>();
        testingList.add(new Spaceship("First", 100, 0, 0));
        testingList.add(new Spaceship("Second", 50, 0, 0));
        Spaceship result = commandCenter.getMostPowerfulShip(testingList);
        return result.getFirePower() == 100;
    }

    private boolean getMostPowerfulShip_returnFirstTest() {
        ArrayList<Spaceship> testingList = new ArrayList<>();
        testingList.add(new Spaceship("First", 100, 0, 0));
        testingList.add(new Spaceship("Second", 100, 0, 0));
        Spaceship result = commandCenter.getMostPowerfulShip(testingList);
        return result.getFirePower() == 100 && result.getName().equals("First");

    }

    private boolean getMostPowerfulShip_NoArmedShipsTest() {
        ArrayList<Spaceship> testingList = new ArrayList<>();
        testingList.add(new Spaceship("First", 0, 0, 0));
        testingList.add(new Spaceship("Second", 0, 0, 0));
        Spaceship result = commandCenter.getMostPowerfulShip(testingList);
        return result == null;
    }

    private boolean getShipByName_shipFoundTest() {
        ArrayList<Spaceship> testingList = new ArrayList<>();
        testingList.add(new Spaceship("First", 100, 0, 0));
        testingList.add(new Spaceship("Second", 50, 0, 0));
        Spaceship result = commandCenter.getShipByName(testingList, "First");
        return result.getName().equals("First");
    }

    private boolean getShipByName_shipNotFoundTest() {
        ArrayList<Spaceship> testingList = new ArrayList<>();
        testingList.add(new Spaceship("First", 100, 0, 0));
        testingList.add(new Spaceship("Second", 50, 0, 0));
        Spaceship result = commandCenter.getShipByName(testingList, "Third");
        return result == null;
    }

    private boolean getAllShipsWithEnoughCargoSpace_returnListTest() {
        ArrayList<Spaceship> testingList = new ArrayList<>();
        testingList.add(new Spaceship("First", 100, 100, 0));
        testingList.add(new Spaceship("Second", 50, 50, 0));
        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testingList, 80);
        return result.get(0).getCargoSpace() >= 80;
    }

    private boolean getAllShipsWithEnoughCargoSpace_returnEmptyListTest() {
        ArrayList<Spaceship> testingList = new ArrayList<>();
        testingList.add(new Spaceship("First", 100, 100, 0));
        testingList.add(new Spaceship("Second", 50, 50, 0));
        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testingList, 200);
        return result.isEmpty();
    }

    private boolean getAllCivilianShips_returnListTest() {
        ArrayList<Spaceship> testingList = new ArrayList<>();
        testingList.add(new Spaceship("First", 100, 0, 0));
        testingList.add(new Spaceship("Second", 0, 0, 0));
        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testingList);
        return result.get(0).getFirePower() == 0;
    }

    private boolean getAllCivilianShips_returnEmptyListTest() {
        ArrayList<Spaceship> testingList = new ArrayList<>();
        testingList.add(new Spaceship("First", 100, 0, 0));
        testingList.add(new Spaceship("Second", 50, 0, 0));
        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testingList);
        return result.isEmpty();
    }
}

